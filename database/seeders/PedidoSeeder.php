<?php

namespace Database\Seeders;

use App\Models\Pedido;
use Illuminate\Database\Seeder;

class PedidoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pedido::create([
            "id" => 1,
            "fecha" => '2022-07-15 12:34:12',
            "client_id" => 1,
            "seller_id" => 1
        ]);
        Pedido::create([
            "id" => 100,
            "fecha" => '2022-07-15 12:34:12',
            "client_id" => 1,
            "seller_id" => 1
        ]);

    }
}
