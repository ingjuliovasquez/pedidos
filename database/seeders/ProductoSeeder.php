<?php

namespace Database\Seeders;

use App\Models\Producto;
use Illuminate\Database\Seeder;

class ProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Producto::create([
            "sku" => '100230432',
            "nombre" => 'F_llantas-Sport'
        ]);

        Producto::create([
            "sku" => '11111111',
            "nombre" => 'F_Parabrisas'
        ]);
        Producto::create([
            "sku" => '100230432',
            "nombre" => 'F_Aceite'
        ]);
    }
}
