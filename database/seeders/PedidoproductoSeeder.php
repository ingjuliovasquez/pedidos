<?php

namespace Database\Seeders;

use App\Models\PedidoProducto;
use Illuminate\Database\Seeder;

class PedidoproductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PedidoProducto::create([
            "order_id" => 1,
            "product_id" => 1,
            "quantity" => 10
        ]);
        PedidoProducto::create([
            "order_id" => 100,
            "product_id" => 1,
            "quantity" => 1045
        ]);
    
    }
}
