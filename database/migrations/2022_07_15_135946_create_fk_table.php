<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // FK pedidos <-> clientes, vendedors
        Schema::table('pedidos', function (Blueprint $table) {
            $table->foreign('client_id')->references('id')->on('clientes');
            $table->foreign('seller_id')->references('id')->on('vendedors');
        });

        // FK pedido_productos <-> pedidos, productos
        Schema::table('pedido_productos', function (Blueprint $table) {
        $table->foreign('order_id')->references('id')->on('pedidos');
        $table->foreign('product_id')->references('id')->on('productos');
         });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
