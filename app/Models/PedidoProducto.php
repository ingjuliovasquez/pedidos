<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PedidoProducto extends Model
{
    use HasFactory;
    public $table = 'pedido_productos';
    public $fillable = [
        'id',
        'order_id',
        'product_id',
        'quantity'

    ];
    public $attributes = [
    
    ];


    public function pedido(){
        return $this->belongsTo('App\Models\Pedido', 'order_id', 'id');
    }

    public function producto(){
        return $this->belongsTo('App\Models\Producto', 'product_id', 'id');
    }


    public function producto_sku(){
        return $this->belongsTo('App\Models\Producto', 'product_id', 'id')->where('sku', 100230432);
    }
}

