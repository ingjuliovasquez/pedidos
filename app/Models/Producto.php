<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    use HasFactory;
    public $table = 'productos';
    public $fillable = [
        'id',
        'sku',
        'nombre'
    ];
    public $attributes = [
    
    ];

    public function pedidoProducto(){
        return $this->hasMany('App\Models\PedidoProducto', 'product_id', 'id');
    }


}
