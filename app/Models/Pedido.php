<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    use HasFactory;
    public $table = 'pedidos';
    public $fillable = [
        'id',
        'fecha',
        'client_id',
        'seller_id'

    ];
    public $attributes = [
    
    ];


    public function cliente(){
        return $this->belongsTo('App\Models\Cliente', 'client_id', 'id');
    }

    public function vendedor(){
        return $this->belongsTo('App\Models\Vendedor', 'seller_id', 'id');
    }

    public function pedidoProducto(){
        return $this->hasMany('App\Models\PedidoProducto', 'order_id', 'id');
    }
}
