<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vendedor extends Model
{
    use HasFactory;
    public $table = 'vendedors';
    public $fillable = [
        'id',
        'name'

    ];
    public $attributes = [
    
    ];


    public function pedido(){
        return $this->hasMany('App\Models\Pedido', 'seller_id', 'id');
    }
}
