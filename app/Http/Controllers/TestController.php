<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Pedido;
use App\Models\PedidoProducto;
use App\Models\Producto;
use Database\Seeders\PedidoSeeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class TestController extends Controller
{

    public function getPedido(Request $request)
    {
        DB::beginTransaction();
        try {
            $arr_esc = array();
            $total = array();
            $cantidad = PedidoProducto::with('pedido')
            ->whereBetween('created_at',[Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get();
                        foreach ( $cantidad as  $cp) {
                         array_push($total, array(
                            "cantidad_productos_pedidos" => $cp->quantity,
                        ));
                        }
            $pedidos = Pedido::with('pedidoProducto','cliente')
            ->whereBetween('created_at',[Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get();
            foreach ( $pedidos as  $p) {
                    array_push($arr_esc, array(
                    "id"  => $p->id,
                    "cliente" => $p->cliente->name,
                    "vendedor" =>  $p->vendedor->name,
                    "cantidad_productos_pedidos" => $total
                ));
            }
            return response($arr_esc);
            DB::commit();
                return response()->json(['response' => 'success', 'status' => 1],200);
        } catch (\Throwable $th) {
            DB::rollback();
         
            return $th;
        }

    }

    public function getProducto(Request $request)
    {
        $pedidos = PedidoProducto::with('producto')->where('order_id',100)->get();
        return response($pedidos);

    }

    public function getPedidoSemana(Request $request)
    {
        $pedidos = Pedido::with('pedidoProducto.producto_sku')
                         ->whereBetween('created_at',[Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get();
        return response($pedidos);
    }

   
}
